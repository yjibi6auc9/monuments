﻿from django.conf.urls import patterns, include, url
from django.contrib import admin
import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'maps.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('monuments.urls')),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',
        {'document_root':'D:/work/maps/Scripts/maps/maps/files/media'}),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
