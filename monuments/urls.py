﻿from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from monuments.views import PointListView


 
urlpatterns = patterns('monuments.views',
    url(r'^$', 'index', name='points-index'), # главная страница
    url(r'^map/get-apartments/$', 'get_apartments', name='get-apartments'), # фильтр главной страницы
    url(r'^map/apartments/filter/$', 'apartments_filter', name='apartments_filter'), # станица с результатом поиска
    url(r'^monument/([0-9]+)?/$', 'monument', name='points-index'), # подробная информация о памятнике
    url(r'^monuments/$', PointListView.as_view(), name='List'), # список всех памятников
)