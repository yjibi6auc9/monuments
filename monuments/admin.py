﻿# -*- coding: utf-8 -*-
from models import *
from django.contrib.gis import admin
from django import forms
from django.db import models
from leaflet.admin import LeafletGeoAdmin
from leaflet.forms.widgets import LeafletWidget

# таблицы доступные для редактирования
 
admin.site.register(Point, LeafletGeoAdmin)
admin.site.register(Author)
admin.site.register(Act)

# создание leaflet карты

class Point(forms.ModelForm): 
    class Meta:
        model = Point
        fields = ('name', 'geometry')
        widgets = {'geometry': LeafletWidget()}
 