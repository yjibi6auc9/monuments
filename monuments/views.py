﻿from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.loader import render_to_string 
# отрисовка страниц
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core import serializers 
# импорт сохранения в json
from django.core.paginator import Paginator 
# импорт добавления страниц
from pure_pagination import Paginator 
# импорт добавления страниц (модификация)
from django.views.generic.list import ListView 
# импорт постраничного отображения
from pure_pagination.mixins import PaginationMixin 
# импорт случайного порядка при выводе на страницу
from monuments.models import * # импорт моделей
from django.contrib.gis import forms

class PointListView(PaginationMixin, ListView):
    model = Point
    template_name = 'points/monuments.html'
    paginate_by = 2

def index(request):
    points = Point.objects.order_by('name')
    return render_to_response('points/index.html', { 
        'points': points,
    })
    
    
def monuments(request):
    points = Point.objects.all()
    time_facilities = Time_facilities.objects.order_by('name')
    styles = Styles.objects.order_by('name')
    boroughs = Borough.objects.order_by('name')
    types = Type.objects.order_by('name')
    values = Values.objects.order_by('name')
    author = Author.objects.order_by('name')
    return render_to_response('points/monuments.html', { 
        'points': points,
        'time_facilities': time_facilities,
        'styles': styles,
        'boroughs': boroughs,
        'types': types,
        'values': values,
        'author': author,
    })

def monument(request, point_id):
    point = Point.objects.get(id=point_id)
    return render_to_response('points/monument.html', { 
        'point': point,
    })	
        
@api_view(['GET'])
def get_apartments(request):
	result = Point.objects.all()
	data = serializers.serialize('json', result)
	return Response(data, status=status.HTTP_200_OK, content_type='application/json')

@api_view(['GET'])
def apartments_filter(request):
	request_data = request.QUERY_PARAMS
	filtered_fields = request_data['fields']

	kwargs = {}
	if "borough" in filtered_fields:
		kwargs['borough'] = request_data['borough']
	if "time_facilities" in filtered_fields:
		kwargs['time_facilities'] = request_data['time_facilities']
	if "values" in filtered_fields:
		kwargs['values'] = request_data['values']
	if "type" in filtered_fields:
		kwargs['type'] = request_data['type']
	if "accessory" in filtered_fields:
		kwargs['accessory'] = request_data['accessory']
	if "styles" in filtered_fields:
		kwargs['styles'] = request_data['styles']
	if "id" in filtered_fields:
		kwargs['id'] = request_data['id']
    
        
	try:
		result = Point.objects.filter(**kwargs)
		data = serializers.serialize('json', result)
		return Response(data, status=status.HTTP_200_OK, content_type='application/json')
		
	except:
		return Response(status=status.HTTP_400_BAD_REQUEST)
        
        


def search_form(request):
    time_facilities = Time_facilities.objects.order_by('name')
    styles = Styles.objects.order_by('name')
    boroughs = Borough.objects.order_by('name')
    types = Type.objects.order_by('name')
    values = Values.objects.order_by('name')
    author = Author.objects.order_by('name')
    return render_to_response('search_form.html', { 

        'time_facilities': time_facilities,
        'styles': styles,
        'boroughs': boroughs,
        'types': types,
        'values': values,
        'author': author,
    })
    
    
def search(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        points = Point.objects.filter(name__icontains=q)
        return render_to_response('points/monuments.html',
            {'points': points, 'query': q})
    else:
        points = Point.objects.all()
        return render_to_response('points/monuments.html', {
            'points': points,
            })