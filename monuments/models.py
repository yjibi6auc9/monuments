﻿# Import django modules
# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.contrib.gis import admin
from django.utils import timezone
 
class Values(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Категория историко-культурного значения объекта культурного наследия")
    class Meta:
        verbose_name = "Категория историко-культурного значения объекта культурного наследия"
        verbose_name_plural = "Категории историко-культурного значения объектов культурного наследия"
    def __unicode__(self):
        return self.name
 
 
class Type(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Вид объекта культурного наследия")
    class Meta:
        verbose_name = "Вид объекта культурного наследия"
        verbose_name_plural = "Виды объектов культурного наследия"
    def __unicode__(self):
        return self.name


class Accessory(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Общая видовая принадлежность объекта культурного наследия")
    class Meta:
        verbose_name = "Общая видовая принадлежность объекта культурного наследия"
        verbose_name_plural = "Общая видовая принадлежность объектов культурного наследия"
    def __unicode__(self):
        return '%s' % (self.name)
        
class Use(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Использование объекта культурного наследия или пользователь")
    class Meta:
        verbose_name = "Использование объекта культурного наследия или пользователь"
        verbose_name_plural = "Использование объектов культурного наследия или пользователи"
    def __unicode__(self):
        return '%s' % (self.name)
        
class Historical_authenticity(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Историческая подлинность")
    class Meta:
        verbose_name = "Историческая подлинность"
        verbose_name_plural = "Историческая подлинность"
    def __unicode__(self):
        return '%s' % (self.name)

class Author(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Автор")
    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"
    def __unicode__(self):
        return '%s' % (self.name)

class Historical_function(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Историческая функция")
    class Meta:
        verbose_name = "Историческая функция"
        verbose_name_plural = "Исторические функции"
    def __unicode__(self):
        return '%s' % (self.name)        
        
class Time_facilities(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Время сооружения памятника")
    class Meta:
        verbose_name = "Время сооружения памятника"
        verbose_name_plural = "Времена сооружения памятников"
    def __unicode__(self):
        return '%s' % (self.name)  
        
class Styles(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Архитектурный стиль здания")
    class Meta:
        verbose_name = "Архитектурный стиль здания"
        verbose_name_plural = "Архитектурные стили зданий"
    def __unicode__(self):
        return '%s' % (self.name)

class Functional_division(models.Model):

    name = models.CharField(max_length=160, verbose_name= "Функциональное деление")
    class Meta:
        verbose_name = "Функциональное деление"
        verbose_name_plural = "Функциональные деления"
    def __unicode__(self):
        return '%s' % (self.name)

class Type_restoration(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Виды ремонтно-реставрационных работ")
    class Meta:
        verbose_name = "Виды ремонтно-реставрационных работ"
        verbose_name_plural = "Виды ремонтно-реставрационных работ"
    def __unicode__(self):
        return '%s' % (self.name)         
   
class Borough(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Район")
    class Meta:
        verbose_name = "Район"
        verbose_name_plural = "Районы"
    def __unicode__(self):
        return '%s' % (self.name) 

class Act(models.Model):

    name = models.CharField(max_length=70, verbose_name= "Акт")
    comment = models.TextField(verbose_name= "Комментарий", null=True, blank=True)
    class Meta:
        verbose_name = "Акт"
        verbose_name_plural = "Акты"
    def __unicode__(self):
        return '%s' % (self.name)         
        
class Point(models.Model):
    
    number = models.IntegerField(verbose_name = "Регистрационный номер", null=True, blank=True, unique=True)
    name = models.TextField(verbose_name = "Наименование", null=True, blank=True)
    date_construction = models.CharField(max_length=70, verbose_name = "Дата сооружения", null=True, blank=True)
    time_facilities = models.ForeignKey(Time_facilities, verbose_name = "Время сооружения памятника", null=True, blank=True)
    author = models.ForeignKey(Author, verbose_name = "Автор", null=True, blank=True)
    location = models.TextField(verbose_name = "Местонахождение", null=True, blank=True)
    borough = models.ForeignKey(Borough, verbose_name = "Район", null=True, blank=True)
    geometry = models.PointField(srid=4326, verbose_name = "Карта")
    values = models.ForeignKey(Values, verbose_name = "Категория значения", null=True, blank=True)
    type = models.ForeignKey(Type, verbose_name = "Вид объекта культурного наследия", null=True, blank=True)
    accessory = models.ForeignKey(Accessory, verbose_name = "Принадлежность", null=True, blank=True)
    attachment = models.BooleanField(verbose_name = "Принадлежность к жилому фонду")
    functional_division = models.ForeignKey(Functional_division, verbose_name = "Функциональное деление", null=True, blank=True)
    historical_function = models.ForeignKey(Historical_function, verbose_name = "Историческая функция", null=True, blank=True)
    historical_authenticity = models.ForeignKey(Historical_authenticity, verbose_name = "Историческая подлинность", null=True, blank=True)
    type_restoration = models.ForeignKey(Type_restoration, verbose_name = "Виды ремонтно-реставрационных работ", null=True, blank=True)
    styles = models.ForeignKey(Styles, verbose_name = "Архитектурные стили зданий", null=True, blank=True)
    decoration = models.TextField(verbose_name = "Характер декора, интерьеров", null=True, blank=True)
    use = models.ForeignKey(Use, verbose_name = "Использование объекта культурного наследия или пользователь", null=True, blank=True)
    history = models.TextField(verbose_name = "Краткие исторические сведения об объекте культурного наследия", null=True, blank=True)
    archive = models.TextField(verbose_name = "Основная библиография и архивные источники об объекте культурного наследия", null=True, blank=True)
    act = models.ForeignKey(Act, verbose_name = "Акт", null=True, blank=True)
    literature = models.CharField(max_length=70, verbose_name = "Регистрационный номер литературы", null=True, blank=True)
    description = models.TextField(verbose_name = "Описание объекта культурного наследия", null=True, blank=True)
    protection = models.TextField(verbose_name = "Предмет охраны объекта культурного наследия", null=True, blank=True)
    boundaries_description = models.TextField(verbose_name = "Описание границ территории объекта культурного наследия и и правового режима земельных участков в указаных границах", null=True, blank=True)
    conclusion = models.TextField(verbose_name = "Общий вывод об историко-культурной ценности объекта культурного наследия", null=True, blank=True)
    state_register = models.TextField(verbose_name = "Наименование, дата, № решения органа гос. Власти о включении объекта культурного наследия в единый гос реестр", null=True, blank=True)
    borderlines = models.TextField(verbose_name = "Наименование, дата, № решения органа гос. Власти об утверждении границ территории объекта культурного наследия", null=True, blank=True)
    federal_monument = models.CharField(max_length=70, verbose_name = "Документ об отнесении памятника к объектам федерального значения", null=True, blank=True)
    world_heritage = models.TextField(verbose_name = "Сведенья о включении объекта культурного наследия в список в список объектов всемирного наследия, либо об отнесении к особо ценным объектам", null=True, blank=True)
    protection_zones = models.TextField(verbose_name = "Наименование, дата и номер акта (актов) органов гос. Власти об утверждении границ зон охраны объекта культурного наследия", null=True, blank=True)
    date_placement = models.TextField(verbose_name = "Данные о размещении сведений об объекте культурного наследия, его территории и границах зон охраны", null=True, blank=True)
    accounting = models.TextField(verbose_name = "Сведения об учете объекта культурного наследия и земельных участков в границах его территории в гос. Кадастре объектов культурного наследия", null=True, blank=True)
    registration = models.TextField(verbose_name = "Сведения о регистрации прав на объект культурного наследия и земельные участки в границах территории в гос. Реестре", null=True, blank=True)
    security_obligation = models.TextField(verbose_name = "Сведения об охранном обязательстве, охранно-арендном договоре или охранном договоре", null=True, blank=True)
    doc_protection = models.CharField(max_length=70, verbose_name = "Номер и дата документа о принятии на охрану", null=True, blank=True)
    photo = models.ImageField(upload_to ='%f' ,verbose_name = "Фото", null=True, blank=True, default='/static/images/no-photo.jpg')
    photo_old = models.ImageField(upload_to ='%f' ,verbose_name = "Старое фото", null=True, blank=True, default='/static/images/no-photo.jpg')
    model = models.FileField(upload_to ='%f' ,verbose_name = "3D модель", null=True, blank=True)
    class Meta:
        verbose_name = "Памятник"
        verbose_name_plural = "Памятники"
    def __unicode__(self):
        return '%s' % (self.name)