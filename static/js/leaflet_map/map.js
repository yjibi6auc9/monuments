/* Define base layers */
var cycleURL='http://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png';
var cycleAttrib='Map data © OpenStreetMap contributors';
var opencyclemap = new L.TileLayer(cycleURL, {attribution: cycleAttrib}); 

var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var osmAttrib='Map data © openstreetmap contributors';
var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib}); 

/* create new layer group */
var layer_apartments = new L.LayerGroup();
var array_markers = new Array();

/* create custom marker which will represent apartments in layer 'layer_apartments' */
customMarker = L.Marker.extend({
   options: { 
      title: 'Monument',
   }
});

/* define function which adds markers from array to layer group */
function AddPointsToLayer() {
    for (var i=0; i<array_markers.length; i++) {
        array_markers[i].addTo(layer_apartments);
    }
} 

/* получение всех параметров */
$.ajax({
	url: '/map/get-apartments/',
	type: 'GET',
	success: function(response) {
        $.each(eval(response), function(key, val) {	  
              	
        	var fields = val.fields; 
        	var regExp = /\(([^)]+)\)/;
			var matches = regExp.exec(fields.geometry);
			var point = matches[1];
			var lon=point.split(' ')[0];
			var lat=point.split(' ')[1];

        	//Создание маркера
        	marker = new customMarker([lat, lon], {
			    title: fields.name,
			    opacity: 1.0  
			}); 
			marker.bindPopup("<strong>"+ fields.name + "</strong><br><strong>" + fields.location + "</strong>");
        	marker.addTo(map);
        	array_markers.push(marker);
        });
        // Добавление маркера
        AddPointsToLayer();
    }
});

/* Основные настройки карты */
var map = L.map('map', {
	center: [47.2313500, 39.7232800], /* начальные координаты */
	zoom: 13,                         /* начальный зум карты */
	fullscreenControl: true,          /* Полноэкранный режим */
	fullscreenControlOptions: {       /* местоположение full screen */
		position: 'topleft'
	},
	layers: [osm, layer_apartments]   /* название слоя */
});

var baseLayers = {
	"OpenCycleMap": opencyclemap,
	"OpenStreetMap": osm
};

var overlays = {
};

L.control.layers(baseLayers).addTo(map);


/* S I D E B A R   F I L T E R */
var sidebar = L.control.sidebar('sidebar', {
    position: 'left'
});
map.addControl(sidebar);

setTimeout(function () {
    sidebar.show();
}, 300);

$('#filter_control').click(function() {
    sidebar.show();
});




/* Настройка фильтра */
function getResult() {
	var selected_borough = $("#select_borough").val();
	var selected_time_facilities = $("#slider_time_facilities").val();
	var selected_values = $("#select_values").val();
	var selected_styles = $("#select_styles").val();
    var selected_type = $("#select_type").val();
    var selected_time_facilities = $("#select_time_facilities").val();
    var selected_accessory = $("#select_accessory").val();

	var fields = new Array();

	if (selected_borough !== 'all') {
		fields.push("borough");
	}

	if (selected_values !== 'all') {
		fields.push("values");
	}
	if (selected_type !== 'all') {
		fields.push("type");
	}
    if (selected_time_facilities !== 'all') {
		fields.push("time_facilities");
	}
    if (selected_styles !== 'all') {
		fields.push("styles");
	}
    if (selected_accessory !== 'all') {
		fields.push("accessory");
	}

	/* ajax call to get all apartments with defined filter values */
	$.ajax({
		url: '/map/apartments/filter/',
		type: 'GET',
		data: "borough=" + selected_borough + "&time_facilities=" + selected_time_facilities + "&values=" 
		+ selected_values + "&styles=" + selected_styles + "&accessory=" + selected_accessory + "&type=" + selected_type + "&fields=" + fields,
		success: function(response) {
			// first delete all markers from layer apartments
			array_markers.length=0;
	        layer_apartments.clearLayers();

	        $.each(eval(response), function(key, val) {	  
	        	//fields in JSON that was returned      	
	        	var fields = val.fields; 

	        	// parse point field to get values of latitude and longitued
	        	var regExp = /\(([^)]+)\)/;
				var matches = regExp.exec(fields.geometry);
				var point = matches[1];
				var lon=point.split(' ')[0];
				var lat=point.split(' ')[1];

	        	//function which creates and adds new markers based on filtered values
	        	marker = new customMarker([lat, lon], {
				    title: fields.name,
				    opacity: 1.0  
				}); 
	        	marker.addTo(map);
	        	marker.bindPopup("<strong>"+ fields.name + "</strong><br><strong>" + fields.location + "</strong><br><strong><a href=http://127.0.0.1:8000/monument/"+ fields.id +"/>"+ fields.id +"</a></strong>");
	        	array_markers.push(marker);
	        });

	        // add markers to layer and add it to map
	        AddPointsToLayer();
	    }
	});
}
